/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_host.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//#include "usbh_cdc.h"
#include <string.h>
//#include "usbh_core.h"
//#include "usbh_def.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
extern ApplicationTypeDef Appli_state;
//extern USBH_HandleTypeDef hUsbHostFS;

//extern USBH_StatusTypeDef USBH_ReEnumerate(USBH_HandleTypeDef *phost);
void UART_Tx_Array(UART_HandleTypeDef *huart3, unsigned int *int_array, uint8_t array_len);
//void USB_Tx_Array(USBH_HandleTypeDef *phost, UART_HandleTypeDef *huart3, unsigned int *int_array, uint8_t array_len);

//USBH_StatusTypeDef USBH_infoDP5(USBH_HandleTypeDef *phost, UART_HandleTypeDef *huart3);
//void connect_amptek(USBH_HandleTypeDef *phost, UART_HandleTypeDef *huart3, uint8_t *data);
//void preamp_func(USBH_HandleTypeDef *phost, UART_HandleTypeDef *huart3, uint8_t preamp_state);
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

ETH_HandleTypeDef heth;

UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ETH_Init(void);
static void MX_USART3_UART_Init(void);
void MX_USB_HOST_Process(void);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  unsigned char buf[32];
  //uint8_t int_buf[]={0xF5};
  //USBH_StatusTypeDef USB_status;
  //uint8_t myBuf[20] = "Hello World \r\n";
  unsigned int int_array[]={0xF5,0xFA,0x03,0x07,0x00,0x00,0xFE,0x07};
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ETH_Init();
  MX_USART3_UART_Init();
  MX_USB_HOST_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
    MX_USB_HOST_Process();

    /* USER CODE BEGIN 3 */
    userFunction();
    //USBH_CDC_SETLineCoding(&hUsbHostFS, );
    //uint8_t *data = (uint8_t *) malloc(1024);
    //connect_amptek(&hUsbHostFS, &huart3, data);
    //USBH_CDC_Transmit(&hUsbHostFS, (uint8_t*) int_array[0], 1);
    //UART_Tx_Array(&huart3, int_array, 8);
    //USB_Tx_Array(&hUsbHostFS, &huart3, int_array, 1);
    //hUsbHostFS

    switch(Appli_state){
     case APPLICATION_IDLE:
       strcpy((char*)buf, "IDLE! \r\n");
       HAL_UART_Transmit(&huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
       HAL_Delay(50); // 50 ms
       break;
     case APPLICATION_START:
       strcpy((char*)buf, "START! \r\n");
       HAL_UART_Transmit(&huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
       HAL_Delay(50); // 50 ms
       HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET); // Allumer LED vert
       /*USB_status = USBH_infoDP5(&hUsbHostFS, &huart3);
       if (USB_status == USBH_OK){
    	 strcpy((char*)buf, "USB OK! \r\n");
         HAL_UART_Transmit(&huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
       }*/
       break;
     case APPLICATION_READY:
       strcpy((char*)buf, "READY! \r\n");
       HAL_UART_Transmit(&huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
       HAL_Delay(50); // 50 ms
       break;
     case APPLICATION_DISCONNECT:
       strcpy((char*)buf, "DISCONNECT! \r\n");
       HAL_UART_Transmit(&huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
       HAL_Delay(50); // 50 ms
       HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET); // Éteindrer LED vert
       break;
    }


    /*preamp_func(&hUsbHostFS, &huart3, 0); // Turn off the preamp
    preamp_func(&hUsbHostFS, &huart3, 1); // Turn on  the preamp
*/
	/*  strcpy((char*)buf, "Before! \r\n");
	  HAL_UART_Transmit(&huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
	  CDC_Transmit_FS( myBuf, strlen((char *)myBuf) );
	  HAL_Delay(500);
	  strcpy((char*)buf, "After! \r\n");
	  HAL_UART_Transmit(&huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);*/



  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability 
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 96;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ETH Initialization Function
  * @param None
  * @retval None
  */
static void MX_ETH_Init(void)
{

  /* USER CODE BEGIN ETH_Init 0 */

  /* USER CODE END ETH_Init 0 */

  /* USER CODE BEGIN ETH_Init 1 */

  /* USER CODE END ETH_Init 1 */
  heth.Instance = ETH;
  heth.Init.AutoNegotiation = ETH_AUTONEGOTIATION_ENABLE;
  heth.Init.PhyAddress = LAN8742A_PHY_ADDRESS;
  heth.Init.MACAddr[0] =   0x00;
  heth.Init.MACAddr[1] =   0x80;
  heth.Init.MACAddr[2] =   0xE1;
  heth.Init.MACAddr[3] =   0x00;
  heth.Init.MACAddr[4] =   0x00;
  heth.Init.MACAddr[5] =   0x00;
  heth.Init.RxMode = ETH_RXPOLLING_MODE;
  heth.Init.ChecksumMode = ETH_CHECKSUM_BY_HARDWARE;
  heth.Init.MediaInterface = ETH_MEDIA_INTERFACE_RMII;

  /* USER CODE BEGIN MACADDRESS */
    
  /* USER CODE END MACADDRESS */

  if (HAL_ETH_Init(&heth) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ETH_Init 2 */

  /* USER CODE END ETH_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LD1_Pin|LD3_Pin|LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(USB_PowerSwitchOn_GPIO_Port, USB_PowerSwitchOn_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin : USER_Btn_Pin */
  GPIO_InitStruct.Pin = USER_Btn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USER_Btn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD1_Pin LD3_Pin LD2_Pin */
  GPIO_InitStruct.Pin = LD1_Pin|LD3_Pin|LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = USB_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(USB_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_OverCurrent_Pin */
  GPIO_InitStruct.Pin = USB_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB_OverCurrent_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

// Function to transmit arrays through UART
void UART_Tx_Array(UART_HandleTypeDef *huart3, unsigned int *int_array, uint8_t array_len){
  /*//Debug
  unsigned char buf[32];
  strcpy((char*)buf, "\n UART Transmit Array \r\n");
  HAL_UART_Transmit(huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
  */
  char buffer[32];
  for (uint8_t i=0; i < array_len; i++){
    HAL_UART_Transmit(huart3, (uint8_t*) buffer, sprintf(buffer,"%x", int_array[i]), HAL_MAX_DELAY); }
}
/*
// Function to transmit arrays through USB
void USB_Tx_Array(USBH_HandleTypeDef *phost, UART_HandleTypeDef *huart3, unsigned int *int_array, uint8_t array_len){
  //Debug
  unsigned char buf[32];
  strcpy((char*)buf, "\n USB Transmit Array \r\n");
  HAL_UART_Transmit(huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);

  char buffer[32];
  for (uint8_t i=0; i < array_len; i++){
    HAL_UART_Transmit(huart3, (uint8_t*) buffer, sprintf(buffer,"%x", int_array[i]), HAL_MAX_DELAY); }
  //USBH_CDC_Transmit(phost, (uint8_t*) int_array[0], 1);
}
*/
/*
// Function to print DP5 info
USBH_StatusTypeDef USBH_infoDP5(USBH_HandleTypeDef *phost, UART_HandleTypeDef *huart3){

  char buffer[32];
  unsigned char buf[32];
  uint8_t idProduct = phost->device.DevDesc.idProduct;
  uint8_t idVendor  = phost->device.DevDesc.idVendor;
  //strcpy((char*)buf, "USB OK! \r\n");
  //HAL_UART_Transmit(huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);


  strcpy((char*)buf, "\n DP5  DevDesc.idProduct: \r\n");
  HAL_UART_Transmit(huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
  HAL_UART_Transmit(huart3, (uint8_t*) buffer, sprintf(buffer,"%x", idProduct), HAL_MAX_DELAY);

  strcpy((char*)buf, "\n DP5  DevDesc.idVendor: \r\n");
  HAL_UART_Transmit(huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
  HAL_UART_Transmit(huart3, (uint8_t*) buffer, sprintf(buffer,"%x", idVendor), HAL_MAX_DELAY);

  return USBH_OK;
}
*/
// Check connection to DP5
/*void connect_amptek(USBH_HandleTypeDef *phost, UART_HandleTypeDef *huart3, uint8_t *data){
	unsigned char buf[32];
  char buffer[32];
  //uint8_t request_netfinder[]={0xF5,0xFA,0x03,0x07,0x00,0x00,0xFE,0x07};
  uint8_t request_netfinder[]={0xF5};

  //HAL_UART_Transmit(huart3, (uint8_t*) buffer, sprintf(buffer,"%x", &request_netfinder), HAL_MAX_DELAY);
  strcpy((char*)buf, "\n *** \r\n");
  HAL_UART_Transmit(huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
  //HAL_UART_Transmit(huart3, (uint8_t*) buffer, sprintf(buffer,"%d", &request_netfinder), HAL_MAX_DELAY);
  HAL_Delay(500); // 500 ms
  uart_write_bytes(UART_NUM_1, (const char *) &request_netfinder, 8);
  int len = uart_read_bytes(UART_NUM_1, dat, BUF_SIZE, 20 / portTICK_RATE_MS);
  if(len>0){
    printf("\nRequest Netfinder: [%u]\n", len);
    for(int j=38;j<60;j++){
      printf("%c", dat[j]);    }
    printf("\n");
    printf("Mac: %x-%x-%x-%x-%x-%x \n", dat[20], dat[21], dat[22], dat[23], dat[24], dat[25]);
  }
  else{
    printf("Error.\n");   }
}
*/
//  Turn on/off preamp
/*void preamp_func(USBH_HandleTypeDef *phost, UART_HandleTypeDef *huart3, uint8_t preamp_state){

  unsigned char buf[32];
  uint8_t paps_on[] ={0xF5,0xFA,0x20,0x02,0x00,0x08,0x50,0x41,0x50,0x53,0x3D,0x4F,0x4E,0x3B,0xFB,0x9E};
  uint8_t paps_off[]={0xF5,0xFA,0x20,0x02,0x00,0x09,0x50,0x41,0x50,0x53,0x3D,0x4F,0x46,0x46,0x3B,0xFB,0x5F};

  if(preamp_state==0){
    strcpy((char*)buf, "\n Turn Off Preamp \r\n");
	HAL_UART_Transmit(huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
	USBH_CDC_Transmit(phost, paps_off, strlen((char *) paps_off));  }
	//USBH_StatusTypeDef  USBH_CDC_Transmit(USBH_HandleTypeDef *phost, uint8_t *pbuff, uint32_t length)
    //uart_write_bytes(UART_NUM_1, (const char *) &paps_off, 17); }
  else if(preamp_state==1){
    strcpy((char*)buf, "\n Turn On Preamp \r\n");
	HAL_UART_Transmit(huart3, buf, strlen((char*)buf), HAL_MAX_DELAY);
	USBH_CDC_Transmit(phost, paps_on, strlen((char *) paps_on));  }
//	uart_write_bytes(UART_NUM_1, (const char *) &paps_on, 16);  }
}

*/
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
