 =========================================================
 Nucleo-F767zi_dev - Development on the Nucleo F767zi board
 =========================================================

                     List of files
                     ---------------
Each folder corresponds to a  project in the STM32CubeIDE.

00_blinky_nucleof767zi
  - Project with  Nucleo board initialization created in STM32CubeMX using the 
    LEDS with the GPIOs peripherals.

01_uart_nucleof767zi
  - Project with  Nucleo board initialization created in STM32CubeMX using the 
    USART3 and GPIOs peripherals (i.e. leds and push buttons).

02_usb_nucleof767zi
  - Project with  Nucleo board initialization created in STM32CubeMX, modifying the
    usb_PowerOn signal (PG6) to enable vbus, using the USB peripheral for a usb 
    drive and the MSC. This project is based on the MYaq00bEmbedded repository:
    https://github.com/MYaqoobEmbedded/STM32-Tutorials

docs
    Documentation used in the development.
    - stm32_nucleo144-boards.pdf: data sheet.   
    - USB_Flash_Tutorial_steps.txt: USB MSC Tutorial taken from 
      https://github.com/MYaqoobEmbedded/STM32-Tutorials and modified to work on 
      the Nucleo-f767zi board.
    - DP5 Programmer's Guide B1.pdf.
    - DP5_LAST_314.txt: Configuration file for DP5  ID: 314.
    - DP5_LAST_362.txt: Configuration file for DP5  ID: 362.

README.md
    Vous êtes en train de le lire maintenant 8-P.

