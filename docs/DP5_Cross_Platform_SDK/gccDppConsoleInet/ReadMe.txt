/** @mainpage


<img alt="Logo" src="vcDP5_CA.PNG"/><h1> gccDppConsoleInet </h1>

gccDppConsoleInet is a console application that demonstrates how Amptek 
DPP communications can be easily integrated into other programs.

<h2> What gccDppConsoleInet does. </h2>
	
gccDppConsoleInet communicates to a DPP device and
demonstrates a minimal set of acquisition related commands.

<h3>gccDppConsoleInet Classes</h3>

- @subpage CConsoleHelper (Simplifies function calls.)
- @subpage CDppSocket (Dpp Sockets communication class.)
- @subpage CDP5Status (DPP status processing.)
- @subpage CParsePacket (DPP packet parsing.)
- @subpage CDP5Protocol (Defines and implements DPP protocol.)
- @subpage CSendCommand (Generates command packet to be sent.)

<h3>gccDppConsoleInet Main Function</h3>

- @subpage _tmain (DPP Console Demonstration.)

<h2> More Information </h2>
 http://amptek.com/products/dp5-digital-pulse-processor-software/ Amptek Software Page

*/



