
DP5 SDK CrossPlatform -- DP5 SDK Version 20140918


Developers notes:

If your libusb library has the libusb_strerror function,
you should comment out CDppLibUsb::libusb_strerror.


vcDppConsoleLibUsb needs the "vc7 runtime" DLLs to run

Redistributables are in the \CrossPlatform\vc7_runtime directory.
