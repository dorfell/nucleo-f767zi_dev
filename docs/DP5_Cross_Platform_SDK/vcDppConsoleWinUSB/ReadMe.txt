/** @mainpage


<img alt="Logo" src="vcDP5_CA.PNG"/><h1> vcDppConsole </h1>

vcDppConsole is a console application that demonstrates how Amptek 
DPP communications can be easily integrated into other programs.

<h2> What vcDppConsole does. </h2>
	
vcDppConsole communicates to a DPP device and
demonstrates a minimal set of acquisition related commands.

<h3>vcDppConsole Classes</h3>

- @subpage CConsoleHelper (Simplifies function calls.)
- @subpage CDppWinUSB (Windows WinUSB communication class.)
- @subpage CDP5Status (DPP status processing.)
- @subpage CParsePacket (DPP packet parsing.)
- @subpage CDP5Protocol (Defines and implements DPP protocol.)
- @subpage CSendCommand (Generates command packet to be sent.)

<h3>vcDppConsole Main Function</h3>

- @subpage _tmain (DPP Console Demonstration.)

<h2> More Information </h2>
 http://amptek.com/products/dp5-digital-pulse-processor-software/ Amptek Software Page

*/



