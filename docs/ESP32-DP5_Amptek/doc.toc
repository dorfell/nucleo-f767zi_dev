\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n}{2}{section.1}% 
\contentsline {section}{\numberline {2}Sistema con Raspberry Pi}{2}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Raspberry Pi}{3}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Amptek DP5}{4}{subsection.2.2}% 
\contentsline {section}{\numberline {3}Plataformas alternativas a Raspberry Pi}{4}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Onion Omega2}{4}{subsection.3.1}% 
\contentsline {subsection}{\numberline {3.2}ESP32}{4}{subsection.3.2}% 
\contentsline {subsection}{\numberline {3.3}Nucleo-f767zi}{6}{subsection.3.3}% 
\contentsline {section}{\numberline {4}Evaluaci\IeC {\'o}n de alternativas}{6}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Onion Omega2}{6}{subsection.4.1}% 
\contentsline {subsection}{\numberline {4.2}ESP32}{6}{subsection.4.2}% 
\contentsline {subsubsection}{\numberline {4.2.1}Verificaci\IeC {\'o}n de interfaz RS-232 de DP5}{6}{subsubsection.4.2.1}% 
\contentsline {subsubsection}{\numberline {4.2.2}Prueba b\IeC {\'a}sica de escritura/lectura por puerto UART en el ESP32}{13}{subsubsection.4.2.2}% 
\contentsline {subsubsection}{\numberline {4.2.3}Implementaci\IeC {\'o}n de c\IeC {\'o}digo para comunicaci\IeC {\'o}n con comandos seriales b\IeC {\'a}sicos}{16}{subsubsection.4.2.3}% 
\contentsline {subsubsection}{\numberline {4.2.4}Medici\IeC {\'o}n y estimaci\IeC {\'o}n de tiempos en las comunicaciones}{19}{subsubsection.4.2.4}% 
\contentsline {subsection}{\numberline {4.3}Implementaci\'on de la comunicaci\'on USB entre la Nucleo-f767zi y la DP5}{24}{subsection.4.3}% 
\contentsline {subsubsection}{\numberline {4.3.1}Identificaci\'on perif\'erico en la DP5}{24}{subsubsection.4.3.1}% 
\contentsline {subsubsection}{\numberline {4.3.2}Librer\'ia USB Host de STMicroelectronics}{26}{subsubsection.4.3.2}% 
\contentsline {subsubsection}{\numberline {4.3.3}Configurando la Nucleo-f767zi como USB Host}{28}{subsubsection.4.3.3}% 
\contentsline {subsubsection}{\numberline {4.3.4}Depuraci\'on del estado APPLICATION\_START}{31}{subsubsection.4.3.4}% 
\contentsline {section}{\numberline {5}Conclusiones}{34}{section.5}% 
