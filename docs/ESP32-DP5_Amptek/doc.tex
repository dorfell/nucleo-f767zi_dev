\documentclass[letterpaper, 11pt]{article}

%Paquetes
\usepackage{dirtree}
\usepackage{afterpage}
\usepackage{pdflscape}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath,amssymb,latexsym}
\usepackage{graphicx}
\usepackage{here}
\usepackage{cite}
\usepackage{pstricks}
%\usepackage{pdfpages}
\usepackage{tikz, pgf}
\setcounter{enumi}{4}
\usetikzlibrary{babel}
\usetikzlibrary{decorations.pathmorphing,shapes.arrows,arrows.meta,positioning}
\usepackage{subcaption}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{color}
\usepackage{xcolor} % Table Color
\usepackage{colortbl}  % Table color
\usepackage{hyperref}
%\usepackage[tiny, md]{titlesec}

\renewcommand{\familydefault}{cmss}
\renewcommand{\seriesdefault}{m}
\renewcommand{\shapedefault}{n}
\renewcommand{\thefootnote}{\arabic{footnote}} %Numeracion de pie de pagina con numeros.
\renewcommand{\figurename}{Fig.} %Que en lugar de "Figure" diga "Figura".
\renewcommand{\refname}{Referencias} %Que en lugar de "References" diga "Referencias".
%\renewcommand{\theenumi}{(\alph{enumi})}

\setlength{\textwidth}{17cm}
\setlength{\textheight}{20cm}
\setlength{\evensidemargin}{0.0cm}
\setlength{\oddsidemargin}{0.0cm}

\usepackage[spanish]{babel}
\decimalpoint
\renewcommand\spanishtablename{Tabla}

\usepackage{hyperref}
\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}

\title{Documentación de evaluación de plataformas alternativas a Raspberry Pi en la comunicación con tarjetas de AMPTEK (DP5).}
%Informacion de autores
\author{
 Ing. Nelson Leonardo Tovar\\ 
 \texttt{\textit{nltovaro@unal.edu.co}} 
 \and 
 Ing. Alexis Cuero Losada\\  
  \texttt{\textit{acuerol@unal.edu.co}}
\and 
 Ing. Dorfell Parra\\  
\texttt{\textit{dlparrap@unal.edu.co}} 
}
\date{\today}

\usetikzlibrary{calc,arrows,decorations.pathmorphing,intersections}
\usepackage[font={small,sf},labelfont={bf},labelsep=endash]{caption}
\usepackage{sansmath}

\usepackage{listings}

% Definiciones de lstlisting
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=bash,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

%% Comienza el documento.
\begin{document}

\maketitle
\tableofcontents

\newpage
\section{Introducción}

En el desarrollo del prototipo funcional del Localizador RNT se ha intensificado la optimización de los componentes hardware y software, principalmente del módulo de adquisición basado en las tarjetas DP5+PC5 fabricadas por AMPTEK y los respectivos complementos. El diseño inicial provee una tarjeta Raspberry Pi para ejecución de un servidor al cual puede conectarse un cliente (mediante Wifi y un protocolo TCP/IP) y la comunicación via USB (\textit{Universal Serial Bus}) con dispositivos como tarjetas DP5 (Digital Pulse Processor) y tarjetas Arduino (para el control de la robótica del prototipo). El cliente puede ser un computador o sistema similar que pueda correr una aplicación desarrollada en Python, y que necesariamente tenga conexión WiFi y soporte el protocolo mencionado. Por otro lado, el servidor debe tener esas dos últimas características del cliente y además debe tener implementado un mecanismo de comunicación con el módulo de adquisición de espectros (que se basa en las tarjetas DP5 de AMPTEK). La aplicación del servidor que incluye esas características no necesariamente debe estar desarrollada en Python, es posible implementarla en otros lenguajes y correrla en otras plataformas. Esto plantea la suposición de que no es necesario utilizar una Raspberry Pi y qué es posible reemplazarla por otra tarjeta que se ajuste mejor a la aplicación y además que sea de menor costo. Para intentar dar respuesta a la suposición anterior, se sugirieron 3 plataformas alternativas:
\begin{itemize}
\item Tarjeta basada en Linux Onion Omega2 (Asignada a Ing. Alexis Cuero)
\item Módulo SoC ESP32 (Asignada a Ing. Nelson Tovar)
\item Unidad de Microcontrolador STM32 (Asignada a Ing. Dorfell Parra)
\end{itemize}
para las cuales se evaluarán aspectos como el desempeño en la comunicación y en el control de las tarjetas DP5 y el costo de implementación de hardware/software respectivo. Este documento tiene como finalidad reportar los resultados y avances de las evaluaciones realizadas a cada una de las plataformas alternativas a la Raspberry Pi.

\section{Sistema con Raspberry Pi}

El sistema actual del Localizador RNT está compuesto por una Raspberry Pi, 2 conjuntos de tarjetas DP5+PC5, 2 preamplificadores y 2 detectores, como se observa en la Figura \ref{diagrama-actual}.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.4]{images/diagrama-actual.pdf}
  \caption{Diagrama general del prototipo.}\label{diagrama-actual}
\end{figure}

\subsection{Raspberry Pi}
La tarjeta Raspberry Pi es un computador de placa simple, también denominada computadora de bolsillo. Incluye un procesador Broadcom, memoria RAM, GPU, puertos USB, HDMI, Ethernet, pines GPIO, un conector para cámara y un slot para memorias microSD. Por el lado del software, soporta una gran variedad de sistemas operativos, siendo las distribuciones Linux las utilizadas y preferidas en este proyecto. El costo promedio de la tarjeta modelo 2B es de 45 USD. 

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.35]{images/raspberry.jpg}
  \caption{Tarjeta Raspberry Pi.}\label{RPI2}
\end{figure}

\subsection{Amptek DP5}
La tarjeta DP5 es un Procesador Digital de Pulsos de alto desempeño que digitaliza señales de salida de un preamplificador. Ofrece ciertas ventajas sobre sistemas tradicionales, incluyendo un rendimiento mejorado (resolución muy alta, déficit balístico reducido, mayor rendimiento y estabilidad mejorada), flexibilidad mejorada, bajo consumo de energía, pequeño tamaño y bajo costo. Soporta interfaces de comunicación como RS-232, USB y Ethernet. El costo promedio de la tarjeta es de 3500 USD (incluyendo la PC5 que provee voltajes de alimentación para detectores y preamplificadores).

\begin{figure}[H]
  \centering
  \includegraphics[scale=1.3]{images/DP5-PC5.jpg}
  \caption{Tarjetas DP5 y PC5 en sandwich.}\label{amptek}
\end{figure}

El fabricante indica y provee la documentación necesaria para la comunicación con la tarjeta mediante un software propio llamado \textit{DPPMCA}. Dicho software es gratuito, pero solo funciona para sistemas operativos de Windows. Sin embargo, es posible comunicar la tarjeta con plataformas que soporten comunicación USB (en modo Host), serial RS-232 y Ethernet, para las cuales existen librerías de software y descripciones completas de todos los comandos y procedimientos de la tarjeta.


\section{Plataformas alternativas a Raspberry Pi}

Las 3 plataformas alternativas sugeridas para la evaluación fueron:
\begin{enumerate}
\item Tarjeta basada en Linux Onion Omega2
\item Módulo SoC ESP32
\item Tarjeta de desarrollo Nucleo-f767zi.
\end{enumerate}
Las cuales se describen brevemente a continuación.

\subsection{Onion Omega2}

\subsection{ESP32}

El módulo ESP32, puntualmente ESP32-WROVER-B es un poderoso módulo MCU que integra WiFi, Bluetooth y Bluetooth de bajo consumo en un único chip y que apunta a una gran variedad de aplicaciones. Está basado en su antecesor (ESP32-WROOM) con la inclusión de una memoria PSRAM de 8 MB y una memoria FLASH de 8 MB. El costo de este chip en Digikey.com es de 5 USD.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.3]{images/esp32.png}
  \caption{Módulo ESP32.}\label{esp32}
\end{figure}

Otras características del ESP32 son:
\begin{itemize}
\item High-speed SPI
\item Ethernet
\item I$^2$C
\item I$^2$S
\item UART
\item Hall sensors
\item SD card interface
\item Capacitive touch sensors
\item Conector IPEX para antena externa
\end{itemize}

El fabricante Espressif brinda una documentación completa para el uso de estos chips en diseños personalizados ó presentes en tarjetas de desarrollo. Igualmente brinda las herramientas para la compilación de proyectos escritos en C/C++, aunque es posible encontrar desarrollos y firmwares para su utilización con Python3 para microcontroladores (\textit{MicroPython}) y otros lenguajes. Para la evaluación de este dispositivo se utilizará la herramienta nativa ESP-IDF que se puede encontrar en el repositorio oficial de Espressif (\url{https://github.com/espressif/esp-idf}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Nucleo-f767zi
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{nucleo_desc.tex}



\section{Evaluación de alternativas}

Los procedimientos, características y resultados de las evaluaciones realizadas a cada una de las plataformas alternativas se detalla a continuación.

\subsection{Onion Omega2}

\subsection{ESP32}

Para la evaluación del ESP32 en la comunicación con la tarjeta de Amptek DP5, se realizaron las etapas que se describen a continuación.

\subsubsection{Verificación de interfaz RS-232 de DP5}\hypertarget{421}{}

\textbf{Comunicación Serial:}\\

La forma más común de conectar y comunicar las tarjetas DP5 es mediante USB. Sin embargo, es posible establecer comunicación mediante un protocolo serial más intuitivo y fácil de entender e implementar, pero de menor velocidad. El \textit{Recommended Standard 232} ó RS-232 es una interfaz que designa una norma para intercambiar datos binarios de forma serial entre 2 dispositivos. El estándar propone utilizar 9 lineas de comunicación y control, sin embargo basta utilizar 3 para transmitir asíncronamente datos de forma bidireccional (2 lineas unidireccionales y referencia a tierra). Comúnmente se encuentran los conectores y cables DB-9.

Por otro lado los controladores \textit{Universal Asynchronous Receiver/Transmiter} ó UART, permiten ser configurados para admitir diferentes protocolos de comunicación serial, entre ellos el RS-232 qué fija las características de voltaje y tiempo para las señales de transmisión (TX) y recepción (RX). Los niveles lógicos de voltaje para este protocolo son 6 V y -6 V, que difiere de los niveles que se pueden obtener en un microcontrolador (3.3 V ó 5 V y 0 V). Esta diferencia de niveles puede hacer creer que ambos son incompatibles, pero la utilización de circuitos integrados adaptadores de niveles lógicos solucionan el problema, mencionando que la forma de enviar bits (marcadores y tiempos) siempre es la misma. En la Figura \ref{uart} se ejemplifica el envió de 1 byte de forma serial, 1 bit de inicio y 1 bit de parada son configurados para que la identificación de bits sea posible. Note que en total son 10 bits, la tasa de transmisión determinará el tiempo por bit y así mismo la duración total del envió o recepción vía serial.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.6]{images/uart.png}
  \caption{Comunicación serial.}\label{uart}
\end{figure}

\textbf{Conexión DppMCA-DP5:}\\

El sofware desarrollado por Amptek para la adquisición de espectros provenientes de su catálogo de productos, puede obtenerse gratuitamente para el entorno Windows. Al abrir la aplicación se lanza una ventana que permite seleccionar la interfaz de comunicación (USB, RS-232 ó Ethernet) y así mismo detectar y establecer conexión con cualquiera de las tarjetas soportadas. Para la verificación de la comunicación serial con la tarjeta DP5 fue necesario utilizar un adaptador USB a RS-232 (conexión al computador) y un cable adicional con terminación jack de audio de 2.5 mm (entrada en la DP5), como se muestra en la Figura \ref{cable}. Este cable puede desensamblarse para conectar otra terminación RS-232 y/ó conectar un osciloscopio en las señales a modo de observación.

\begin{figure}[H]
  \centering
  \includegraphics[width=8cm]{images/cable.jpg}
  \caption{Cable utilizado para la conexión PC-DP5.}\label{cable}
\end{figure}

Una vez alimentada (mediante un adaptador externo) la tarjeta DP5 y conectada al PC mediante el cable anteriormente mencionado, se puede ejecutar la aplicación DPPMCA. Al seleccionar RS-232 y el puerto respectivo (COM) se debe abrir el puerto para verificar si hay una conexión satisfactoria con la placa. Esta información se mostrará automáticamente en la ventana. Para establecer la conexión se debe oprimir el botón ``Connect'', como se muestra en la Figura \ref{connect}.

\begin{figure}[H]
  \centering
  \includegraphics[width=15cm]{images/connect.png}
  \caption{Ventana de conexión de la aplicación DPPMCA.}\label{connect}
\end{figure}

La información del tipo de tarjeta, serial, dirección MAC, tipo de conexión y tiempo de encendido se muestran en la ventana. Esto corresponde a uno o dos comandos enviados por la aplicación a la tarjeta que veremos más adelante, la tarjeta responde a los comandos con la información solicitada.\\

\textbf{Adquisición de espectros:}\\

Para comprobar el funcionamiento básico de la tarjeta DP5 se emuló el bloque analógico (detector + preamplificador + circuitos de polarización) con un generador de señales (Analog Discovery2 de Digilent, que también funciona como osciloscopio). Una señal cuadrada de 40 mVpp, 10 kHz de frecuencia y 30\% de ciclo útil (ó asimetría), fue generada en el canal 1 del generador y conectada a la entrada analógica J4 de la DP5. La forma de onda se muestra en la Figura \ref{waveform}.

\begin{figure}[H]
  \centering
  \includegraphics[width=15cm]{images/waveform.png}
  \caption{Señal de entrada para emular el bloque analógico.}\label{waveform}
\end{figure}

Los tiempos de adquisición ``Preset Time'' y ``Preset Realtime'' se ajustaron en 10 s, 1 s y 0.1 s. Las Figuras \ref{seg10}, \ref{seg1} y \ref{seg01} muestran respectivamente los espectros adquiridos.

\begin{figure}[H]
  \centering
  \includegraphics[width=15cm]{images/spec-10.png}
  \caption{Espectro obtenido para 10 s de adquisición.}\label{seg10}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=15cm]{images/spec-1.png}
  \caption{Espectro obtenido para 1 s de adquisición.}\label{seg1}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=15cm]{images/spec-01.png}
  \caption{Espectro obtenido para 100 ms de adquisición.}\label{seg01}
\end{figure}

La señal de la Figura \ref{waveform} causa un pico en el canal 327 con cuentas de $\approx$ 20 k para 10 s, $\approx$ 2 k para 1 s y $\approx$ 200 para 0.1 s de adquisición. De esta manera se tiene una referencia para la adquisición de espectros sin utilizar la aplicación DPPMCA con representación gráfica.\\

\textbf{Linea básica de comandos:}\\

La aplicación DPPMCA provee una linea de comandos de tipo texto para enviar y recibir a la tarjeta conectada. Con base en la \href{http://experimentationlab.berkeley.edu/sites/default/files/images/DP5_Programmers_Guide_A4.pdf}{Guia del Programador para tarjeta DP5} (el Capítulo 5 de la guía describe en detalle cada uno de los comandos), se identificaron comandos de texto (ASCII) para la ejecución en la tarjeta DP5, tales como prender/apagar las fuentes de alimentación de los preamplificadores o de alto voltaje, ajuste de parámetros como ``Preset Time'', ``Preset Realtime'', entre otros. 
La ventana de configuración de DPP permite enviar los comandos de texto. Para prender las fuentes de alimentación de los preamplificadores (Pines 4 y 5 del conector J2 de la tarjeta PC5) se envía la cadena de texto ``PAPS=ON;'' como se muestra en la Figura \ref{shell-on}. Un LED conectado directamente a la fuente positiva (Pin 4) sirve como monitor del estado ON/OFF.

\begin{figure}[H]
  \centering
  \includegraphics[width=10cm]{images/shell-on.png}
  \includegraphics[width=6.3cm]{images/LEDON.jpg}
  \caption{Envío de comando de texto para prender las fuentes del preamplificador.}\label{shell-on}
\end{figure}

De igual manera, para apagar las fuentes se emplea el comando ``PAPS=OFF;'' y además es posible solicitar el valor (Lectura del comando) para saber el estado de las fuentes, en este caso. La Figura \ref{shell-off} muestra la lectura del estado y el envío del comando de apagado de las fuentes del preamplificador.

\begin{figure}[H]
  \centering
  \includegraphics[width=10cm]{images/shell-off.png}
  \includegraphics[width=6.3cm]{images/LEDOFF.jpg}
  \caption{Envío de comando de texto para apagar las fuentes del preamplificador y solicitar el estado.}\label{shell-off}
\end{figure}

\subsubsection{Prueba básica de escritura/lectura por puerto UART en el ESP32}

Una vez verificada la comunicación de la DP5 a través de la interfaz serial RS-232 con su aplicación DPPMCA, el siguiente paso fue migrar la implementación de comandos básicos a la plataforma ESP32. Para ello fue necesario realizar una prueba básica de lectura y escritura por uno de sus puertos UART. La documentación para la configuración e instalación del driver UART se puede obtener de su fabricante \href{https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/uart.html}{Espressif}. \\

\textbf{Conexión de un conversor UART a RS-232:}\\

Como se mencionó anteriormente, los niveles de voltaje son diferentes en la implementación del microcontrolador y el protocolo RS-232. Un módulo convertidor como el de la Figura \ref{conversor} fue adquirido y conectado al puerto ``UART\_1'' de un ESP32-Wrover para protoboard.

\begin{figure}[H]
  \centering
  \includegraphics[width=8cm]{images/conversor.jpg}
  \caption{ESP32-Wrover y conversor RS-232.}\label{conversor}
\end{figure}

\textbf{Envio de comandos ``PAPS'' desde el ESP32:}\\

El comando ASCII ``PAPS=value;'' es posible enviarlo en una secuencia de 16 bytes. La siguiente cadena de bytes representan el comando ``PAPS=ON;'' para escribir a través del puerto UART (los valores se representan en hexadecimal):
$$
\textcolor{red}{F5-FA-}\textcolor{green}{20-02-}\textcolor{cyan}{00-08-}\textcolor{blue}{50-41-50-53-3D-4F-4E-3B-}FB-9E
$$
\begin{itemize}
\item Los primeros 2 bytes (en rojo) son comunes en todos los comandos de Amptek, siempre tienen ese valor para comandos de petición y respuesta.

\item Los siguientes 2 bytes (en verde) son identificadores propios del comando. En este caso corresponden a comandos de texto ASCII.

\item Los siguientes 2 bytes (en cyan) representan la longitud de bytes del paquete (comando de texto que sigue inmediatamente).

\item El paquete o comando (en azul) representa cada caracter del comando ``PAPS=ON;'' en ASCII. P=0x50 (ver tabla \href{https://www.ascii-code.com/}{ASCII}) por ejemplo.

\item Los ultimos 2 bytes (en negro) son bytes de verificación o \textit{checksum} de todos los bytes anteriores.
\end{itemize}

De igual manera, la siguiente cadena de bytes representan el comando ``PAPS=OFF;'' para escribir a través del puerto UART (los valores se representan hexadecimálmente), nótese que tiene 1 byte adicional respecto al anterior comando, esto debido a que ``OFF'' tiene un caracter más que ``ON'':
$$
\textcolor{red}{F5-FA-}\textcolor{green}{20-02-}\textcolor{cyan}{00-08-}\textcolor{blue}{50-41-50-53-3D-4F-46-46-3B-}FB-5F
$$

Un sencillo programa para prender y apagar las fuentes del preamplificador fue implementado. Véase el siguiente \href{https://gitlab.com/neutronsaei/naei_sw/-/tree/dev_nelsont/RobertV1_sw/projects/uart_test}{proyecto}.\\

\textbf{Comandos de estado e indentificación de tarjeta:}\\

Los comandos ``Request Netfinder'' y ``Request Status'' son utiles para verificar si hay conexión con la tarjeta DP5. 

De la respuesta al comando ``Request Netfinder'' se puede obtener:
\begin{itemize}
\item Tipo de dispositivo
\item Numero de serie
\item Dirección MAC
\item Tiempo de funcionamiento
    \end{itemize}

Este es un comando de 8 bytes y la respuesta de esta tarjeta es un paquete de respuesta de 109 bytes.\\

De la respuesta al comando ``Request Status'' se puede obtener:
\begin{itemize}
\item Tipo de dispositivo
\item Versión de firmware 
\item Versión de FPGA
\item Numero de serie
\item Estado del alto voltaje
\item Valor del voltaje de fuentes de preamplificador
\item Entre otros
\end{itemize}

Este es un comando de 8 bytes y la respuesta de esta tarjeta es un paquete de respuesta de 72 bytes.\\

\textbf{Comandos de petición de espectros e inicio/fin de adquisición:}\\

Los comandos para solicitar y borrar espectros son de nuestro interés, así como los comandos para iniciar y detener una adquisición. Hay 4 diferentes comandos para los espectros ``Request spectrum'', ``Request \& clear spectrum'', ``Request spectrum + status'' y ``Request \& clear spectrum + status'', todos son comandos de 8 bytes de longitud pero las respuestas de la tarjeta (si se utilizan 512 canales) a estos comandos son de 1544, 1544, 1608 y 1608 bytes respectivamente.\\

Los comandos ``Enable MCA'' y ``Disable MCA'' permiten iniciar y detener una adquisición respectivamente. Son comandos de 8 bytes y la respuesta de la tarjeta es un paquete ``OK'' de 8 bytes también.

\subsubsection{Implementación de código para comunicación con comandos seriales básicos}

La Figura \ref{ESP32-DP5} muestra la conexión entre el ESP32, el conversor RS-232, la tarjeta DP5, el osciloscopio a ambas señales (TX y RX) y el generador a la entrada analógica de la DP5. La tasa de transmisión fue de 115200 baud.

\begin{figure}[H]
  \centering
  \includegraphics[width=8cm]{images/ESP32-DP5.jpg}
  \caption{Montaje para la evaluación del ESP32 con DP5.}\label{ESP32-DP5}
\end{figure}


Se escribieron funciones de software que agrupan commandos por tipo (Espectros, MCA, Fuentes de preamplificador, Status y Netfinder). A cada comando se le asignó una tecla del teclado que puede ser enviada via serial a través del puerto UART\_0, por este mismo puerto se reciben e imprimen en pantalla las respuestas a cada comando.
\begin{itemize}
\item El comando ``Request Netfinder'' puede enviarse al enviar una ``N'' como se muestra en la Figura \ref{netfinder}.
\item El comando ``Request Status'' puede enviarse al enviar una ``S'' como se muestra en la Figura \ref{rq-status}.
\item Las fuentes de preamplificador se pueden encender al enviar una ``P''.
\item Las fuentes de preamplificador se pueden apagar al enviar una ``p''.
\item La adquisición puede iniciarse (habilitar el MCA) al enviar una ``M''.
\item La adquisición puede detenerse (deshabilitar el MCA) al enviar una ``m'' como se muestra en la Figura \ref{MCAS}.
\item Un espectro puede solicitarse al enviar un ``1''.
\item Un espectro + borrado puede solicitarse al enviar un ``2''.
\item Un espectro + status puede solicitarse al enviar un ``3''.
\item Un espectro + status + borrado puede solicitarse al enviar un ``4''.
\end{itemize}

\begin{figure}[H]
  \centering
  \includegraphics[width=11cm]{images/netfinder.png}
  \caption{Respuesta al comando ``Request Netfinder''.}\label{netfinder}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=11cm]{images/rq-status.png}
  \caption{Respuesta al comando ``Request Status''.}\label{rq-status}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=11cm]{images/mcas.png}
  \caption{Respuesta a los comandos ``Enable MCA'' y ``Disable MCA''.}\label{MCAS}
\end{figure}

Para realizar una adquisición y comparar los espectros es necesario borrar el espectro (enviar un ``2''), detener la aduisición (``m'' si estuviera en curso) y apagar las fuentes del preamplificador (``p''). Al solicitar un spectro se debe obneter el espectro vacío (512 canales en 0 decimal y separados por un ``\_'') como lo muestra la Figura \ref{1-spec}.

\begin{figure}[H]
  \centering
  \includegraphics[width=17cm]{images/1-spec.png}
  \caption{Espectro vacío.}\label{1-spec}
\end{figure}

Para empezar la adquisición se debió ajustar los ``Preset Times'' previamente, en este caso se colocaron en 10 s. Despues se encienden las fuentes de preamplificador (``P'') y se inicia la adquisición (``M''), el ESP32 hará un blink de un LED por 10 s para indicar cuando termina la adquisición. Finalmente se solicita el espectro (``1''). La Figura \ref{ADQUIS} muestra las respuestas de la tarjeta DP5 a los anteriores pasos. Se observa que hay un grupo de valores donde el máximo de cuentas registradas fue de 19983 en el canal 327, lo que corresponde a un conteo esperado como se discutió en la Sección \hyperlink{421}{4.2.1} (Figura \ref{seg10}).

\begin{figure}[H]
  \centering
  \includegraphics[width=17cm]{images/ADQUIS.png}
  \caption{Adquisición de 10 s y espectro de respuesta en pantalla.}\label{ADQUIS}
\end{figure}

El programa para realizar la comunicación con los comandos mostrados puede verse en el siguiente \href{https://gitlab.com/neutronsaei/naei_sw/-/tree/dev_nelsont/RobertV1_sw/projects/amptek}{proyecto}.\\

\subsubsection{Medición y estimación de tiempos en las comunicaciones}
Una consideración importante en esta evaluación es la del tiempo empleado para leer/escribir mediante comandos seriales. La estimación de cuanto tiempo toma transmitir 1 bit puede realizarse partiendo del hecho de que la tasa de transmisión serial empleada (\textit{Baudrate}) es equivalente al numero de bit por segundo. El inverso de la tasa de transmisión es el tiempo necesario para transmitir o recibir un único bit.
\begin{equation}\label{eq1}
\frac{1}{BAUDRATE}=\frac{1}{115200 \textrm{ bps}}\approx 8.68\;\mu\textrm{s/bit}
\end{equation}

Ahora bien, para la transmisión de un paquete de 1 byte (8 bits) es necesario añadir 2 bits (uno de inicio y otro de parada ), lo que resulta en la transmisión de 10 bits en un tiempo aproximado de 86.8 $\mu$s. Este valor es escalable, puesto que si son 2 o más bytes seguidos se añadirá un separador de 2 bits (``1-0'') entre bytes, resultando en 10 bits por byte. Por ejemplo, el comando ``Request Status'' (8 bytes) toma:
$$
  8\;\textrm{B}=10\times8\textrm{ bit}=80\textrm{ bit}
$$
$$
  80\textrm{ bit}\times8.68\;\mu\textrm{s/bit}=694.44\;\mu\textrm{s}
$$

En la Figura \ref{osc1} se muestra la medición con el osciloscopio de la señal TX del ESP32 cuando se envía el comando ``Request Status''.

\begin{figure}[H]
  \centering
  \includegraphics[width=17cm]{images/osc1.png}
  \caption{Comando ``Request Status'' en el osciloscopio.}\label{osc1}
\end{figure}

De igual manera se puede calcular el tiempo que toma recibir un paquete, el numero de bytes y el \textit{Baudrate} son inicialmente suficientes. Sin embargo hay otro tiempo que debe tenerse en cuenta (y medirse). Este es el tiempo de respuesta ($t_{resp}$) entre la finalización de un comando de petición transmitido y el inicio de la respuesta de la tarjeta DP5. La Figura \ref{osc2} muestra el comando ``Enable MCA'' y el ``OK'' respondido por la tarjeta, con los cursores verdes se midió el $t_{resp}$. 

\begin{figure}[H]
  \centering
  \includegraphics[width=17cm]{images/osc2.png}
  \caption{Comando ``Enable MCA'' y su respuesta en el osciloscopio.}\label{osc2}
\end{figure}

El tiempo de respuesta $t_{resp}$ es 36 $\mu$s, lo que resulta en 
$$
  t_{total}=2\times t_{(8B)}+t_{resp}\approx730.44\;\mu\textrm{s}
$$
la duración completa de la comunicación para ese comando.\\

\textbf{El problema de longitud de los espectros:}\\

Los tiempos para enviar y recibir paquetes/comandos con longitud de algunos bytes son pequeños comparados al problema que toma recibir un espectro. Los comandos de petición de espectros son de 8 bytes cada uno, pero la respuesta que envía la tarjeta DP5 de vuelta tiene entre 1544 y 1608 bytes. Sin contar el tiempo de respuesta $t_{resp}$, la duración total de recepción de un espectro de 512 canales en 1608 B es:
$$
  t_{RX}=1608\;\textrm{B}\times 10\textrm{ bit/B} \times 8.68\;\mu\textrm{s/bit} \approx 139.6 \textrm{ ms}
$$
siendo un tiempo significativo si se desean tomar varios espectros por segundo. El tamaño de un espectro se debe a que utiliza 3 bytes por canal y otros bytes para representar el Status de la tarjeta.\\

\textbf{Tiempos para los comandos implementados:}\\

La medición y/o cálculo de los tiempos de respuesta y transmisión/recepción de comandos se muestra en la Tabla \ref{tab-time}.

\begin{table}[h]
\centering {\small
\begin{tabular}{|p{3cm}|p{1.2cm}|p{1cm}|p{1.7cm}|p{3cm}|p{1.2cm}|p{1cm}|p{1.7cm}|}\hline
\textbf{RQ Command} & \textbf{Size (bytes)} & \textbf{$t_{TX}$ (ms)} & \textbf{$t_{resp}$ (ms)} &\textbf{Resp. Command} & \textbf{Size (bytes)} & \textbf{$t_{RX}$ (ms)} & \textbf{$t_{total}$ (ms)} \\ \hline
RQ Status & 8 & 0.694 & 0.275 & Resp. Package & 72 & 6.25 & 7.22 \\ \hline
RQ Netfinder & 8 & 0.694 & 0.334 & Resp. Package & 109 & 9.46 & 10.49 \\ \hline
$PAPS=ON$ & 16 & 1.39 & 0 & ---& - & 0 & 1.39 \\ \hline
$PAPS=OFF$ & 17 & 1.48 & 0 & ---& - & 0 & 1.48 \\ \hline
Enable MCA & 8 & 0.694 & 0.038 & OK Package & 8 & 0.694 & 1.43 \\ \hline
Disable MCA & 8 & 0.694 & 0.038 & OK Package & 8 & 0.694 & 1.43 \\ \hline
RQ Spectrum & 8 & 0.694 & 0.672 & Spectrum Package & 1544 & 134.03 & 135.4 \\ \hline
RQ Spectrum + Clear & 8 & 0.694 & 0.672 & Spectrum Package & 1544 & 134.03 & 135.4 \\ \hline
RQ Spectrum + Status & 8 & 0.694 & 0.730 & Spectrum Package & 1608 & 139.6 & 141.02 \\ \hline                       
RQ Spectrum + Status + Clear & 8 & 0.694 & 0.730 & Spectrum Package & 1608 & 139.6 & 141.02 \\ \hline
\end{tabular}}
\caption{Tiempos de transmisión y recepción de los comandos implementados.}
\label{tab-time}
\end{table}

\textbf{Evaluación con 2 puertos de comunicación:}\\

Las mediciones realizadas solo tenían en cuenta la comunicación con 1 tarjeta DP5. Sin embargo, las limitaciones en la velocidad de transmisión crean la necesidad de evaluar una posible implementación con 2 tarjetas, a lo que es necesrio emular estas tarjetas debido a los problemas logísticos del momento. Para ello, se implementó un programa simple que responde ante un comando ``RQ Spectrum'' devolviendo 1544 bytes creados artificialmente con algún pico predeterminado. Este programa se implementó en 2 dispositivos independientes: otro ESP32 y un ESP8266. En el programa de evaluación principal (que se ejecuta en un ESP32) se añadió y configuró otro puerto UART y se realizó la medición del tiempo que le toma al ESP32 enviar 2 ``RQ Spectrum'' y recibir satisfactoriamente 2 espectros de las tarjetas DP5 (en este caso emuladas). Las observaciones fueron las siguientes:
\begin{itemize}
\item El ESP32 es capaz de enviar casi simultaneamente los 2 comandos ``RQ Spectrum'', con una diferencia de 15 $\mu$s entre ambas señales.
\item Las UARTs del ESP32 pueden configurarse para almacenar temporalemnte en un BUFFER (para este caso se configuró en 2024 bytes cada una) los datos entrantes cuando no se está atendiendo al periferico serial. Esto permite no perder datos debido a la ejecución de otras rutinas y disponer de los datos de forma no secuencial.
\item Si bien cada UART puede almacenar los datos en el BUFFER, cuando se obtienen dichos datos a las variables del programa principal se requieren aproximadamente 10 ms (por cada 1.5 kB).
\item Se utilizó un pin GPIO como bandera para medir el tiempo desde que se envía el primer ``RQ Spectrum'' hasta que se tiene en memoria el segundo espectro de respuesta. En la Figura \ref{2spectros} puede observarse que la señal azul (bandera de medición) tiene un ancho de aproximadamente 170 ms. El resultado de esta prueba es la obtención de 2 espectros independientes como se muestra en la Figura \ref{2spec-cod}.

\begin{figure}[H]
  \centering
  \includegraphics[width=17cm]{images/2-spec-osc.png}
  \caption{Medición del tiempo total de comunciacón con 2 tarjetas DP5.}\label{2spectros}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=17cm]{images/2-spec-num.png}
  \caption{Impresión en pantalla de los 2 espectros obtenidos.}\label{2spec-cod}
\end{figure}

\end{itemize}


\textbf{Ventajas de utilizar el ESP32 en la comunicación con la DP5:}\\
\begin{itemize}
\item Fácil implementación de la comunicación serial. Solo requiere un adaptador RS232.
\item Tiene hasta 3 puertos UART. Ideal para conectar 2 tarjetas DP5 con la posibilidad de obtener 5 espectros/s como máximo.
\item Integra comunicación WiFi bajo TCP/IP. Esto permitiría enviar ``inmediatamente'' es espectro obtenido al Cliente (Por medirse).
\item Tiene 4 MB en FLASH/PSRAM (incluso hay versiones de 8 y 16 MB). Si un espectro pesa 1.5 kB podrían almacenarse más de 2000 espectros.
\item Ya está integrado en la placa RobertV1. Aunque deben hacerse modificaciones para utilizarlo en este propósito.
\end{itemize}

\textbf{Desventajas de utilizar el ESP32 en la comunicación con la DP5:}\\
\begin{itemize}
\item La tasa de transmisión máxima es de 115200 baud. Esto significa limitar a 5 espectros/s basados en las mediciones realizadas a 2 conexiones con DP5.
\item La interfaz Ethernet podría evaluarse, pero el número de pines no alcanzaría para implementar 2 puertos Ethernet.
\end{itemize}

\textbf{Posibles soluciones:}\\
\begin{itemize}
\item Utilizar comunicación USB en un microcontrolador esclavo (STM32 ó USB dedicados) y comunicar vía SPI-HS (100 veces más rápido) el ESP32 para transmitir por WiFi los espectros.
\item Realizar mediciones de espectros de larga duración y solicitar/borrar espectros en periodos cortos. Se probó la adquisición de 10 espectros cada 0.2 seg cuando ``PRER-PRET=10 s'', el pico de cuentas fue repetible en 9 de 10 espectros (con $\approx$800 cuentas en el pico).
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% USB communitation between STM32 and DP5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{stm32_dp5_usb.tex}


\section{Conclusiones}

\end{document}
